package main

import "fmt"
import "net/http"
import "os"
import "encoding/json"
import "io/ioutil"
import "time"
import "bytes"

//WebhookResponse response to dialogFlow
type WebhookResponse struct {
	Speech      string   `json:"speech"`
	DisplayText string   `json:"displayText"`
	Data        string   `json:"data"`
	ContextOut  []string `json:"contextOut"`
	Source      string   `json:"source"`
}

//Request request from dialogFlow
type Request struct {
	ID        string    `json:"id"`
	Timestamp time.Time `json:"timestamp"`
	Lang      string    `json:"lang"`
	Result    struct {
		Source           string `json:"source"`
		ResolvedQuery    string `json:"resolvedQuery"`
		Speech           string `json:"speech"`
		Action           string `json:"action"`
		ActionIncomplete bool   `json:"actionIncomplete"`
		Parameters       struct {
			BaseCurrency   string `json:"BaseCurrency"`
			TargetCurrency string `json:"TargetCurrency"`
		} `json:"parameters"`
		Contexts []interface{} `json:"contexts"`
		Metadata struct {
			IntentID                  string `json:"intentId"`
			WebhookUsed               string `json:"webhookUsed"`
			WebhookForSlotFillingUsed string `json:"webhookForSlotFillingUsed"`
			IntentName                string `json:"intentName"`
		} `json:"metadata"`
		Fulfillment struct {
			Speech   string `json:"speech"`
			Messages []struct {
				Type   int    `json:"type"`
				Speech string `json:"speech"`
			} `json:"messages"`
		} `json:"fulfillment"`
		Score float64 `json:"score"`
	} `json:"result"`
	Status struct {
		Code      int    `json:"code"`
		ErrorType string `json:"errorType"`
	} `json:"status"`
	SessionID string `json:"sessionId"`
}

//CurryURL url of the currency exhange service(assignment2)
var CurryURL = "https://protected-ravine-43175.herokuapp.com/ex/latest"

func webhookTest(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		fmt.Println("Ilegal method")
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Printf("IO Error: %s\n", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	m := Request{}
	err = json.Unmarshal(body, &m)
	if err != nil {
		fmt.Printf("JSON Error: %s\n", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	s := ""
	if m.Result.Parameters.BaseCurrency == "" || m.Result.Parameters.TargetCurrency == "" {
		s += "Unrecognizable currency specified"
	} else {
		a := make(map[string]interface{})
		a["baseCurrency"] = m.Result.Parameters.BaseCurrency
		a["targetCurrency"] = m.Result.Parameters.TargetCurrency
		jsonBlob, err := json.Marshal(a)
		if err != nil {
			//fmt.Println(err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		resp, err := http.Post(CurryURL, "application/json", bytes.NewBuffer(jsonBlob))
		if err != nil {
			//fmt.Println(err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		respBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			//fmt.Println(err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		s = "The current exchange rate between " + m.Result.Parameters.BaseCurrency + " and  " + m.Result.Parameters.TargetCurrency + " is " + string(respBody)
	}
	wr := WebhookResponse{}
	wr.Speech = s
	wr.DisplayText = s
	w.Header().Add("Content-type", "application/json")
	json.NewEncoder(w).Encode(wr)
}

func main() {
	port := os.Getenv("PORT")
	newCurryURL := os.Getenv("CURRYURL")
	if port == "" {
		port = "8080"
	}
	if newCurryURL != "" {
		CurryURL = newCurryURL
	}
	fmt.Print("Starting server on port: " + port + "\n")
	http.HandleFunc("/bot", webhookTest)
	http.ListenAndServe(":"+port, nil)
}
