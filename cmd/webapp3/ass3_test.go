package main

import "fmt"
import "net/http"
import "net/http/httptest"
import "encoding/json"
import "bytes"
import "testing"

func Test_AccessAndDelete(t *testing.T) {
	ts1 := httptest.NewServer(http.HandlerFunc(webhookTest))
	defer ts1.Close()
	//Illegal method
	resp, err := http.Get(ts1.URL)
	if err != nil {
		t.Errorf("Error doing http.Get(): %s", err)
	}
	//Malformed json
	resp, err = http.Post(ts1.URL, "application/json", bytes.NewBuffer([]byte("dlfkjsdkj")))
	if err != nil {
		t.Errorf("Error doing http.Get(): %s", err)
	}
	r := Request{}
	//No currency
	r.Result.Parameters.BaseCurrency = ""
	r.Result.Parameters.TargetCurrency = ""
	jsonBlob, err := json.Marshal(r)
	if err != nil {
		fmt.Printf("JSON Error: %s\n", err)
		return
	}
	resp, err = http.Post(ts1.URL, "application/json", bytes.NewBuffer(jsonBlob))
	if err != nil {
		t.Errorf("Error doing http.Get(): %s", err)
	}
	//Success
	r.Result.Parameters.BaseCurrency = "EUR"
	r.Result.Parameters.TargetCurrency = "NOK"
	jsonBlob, err = json.Marshal(r)
	if err != nil {
		fmt.Printf("JSON Error: %s\n", err)
		return
	}
	resp, err = http.Post(ts1.URL, "application/json", bytes.NewBuffer(jsonBlob))
	if err != nil {
		t.Errorf("Error doing http.Get(): %s", err)
	}
	if resp.StatusCode != http.StatusOK {
		t.Error("StatusCode should be OK (200), but was " + fmt.Sprint(resp.StatusCode))
	}
}
