FROM golang:1.9.2-stretch

LABEL maintainer="zohaibb@stud.ntnu.no"

RUN apt-get update && apt-get -y upgrade

# Get the repo for assignment2 and install
RUN go get bitbucket.org/hkonn/imt2681_assignment2/cmd/webapp
RUN go install bitbucket.org/hkonn/imt2681_assignment2/cmd/webapp

# Current working directory
WORKDIR $GOPATH

CMD ["./bin/webapp"]
